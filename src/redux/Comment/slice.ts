import {ICommentsData} from "../../interfaces/interfaces";
import {createSlice, PayloadAction} from "@reduxjs/toolkit";

const initialState: ICommentsData[] = []

const commentSlice = createSlice({
  name: 'comments',
  initialState,
  reducers: {
    addComment(state, {payload}: PayloadAction<{ newComment: ICommentsData }>) {
      const {newComment} = payload

      state.push(newComment)
    },

    changeComment(state, {payload}: PayloadAction<{ commentId: number, newComment: string }>) {
      const {newComment, commentId} = payload

      const editComment = state.find(item => item.id === commentId)

      if (editComment) {
        editComment.comment = newComment
      }
    },

    deleteComment(state, {payload}: PayloadAction<{ commentId: number }>) {
      const {commentId} = payload
      const index = state.findIndex((item) => item.id === commentId);

      if (index !== -1) {
        state.splice(index, 1);
      }
    }
  }
})

const actions = {...commentSlice.actions}
const reducer = commentSlice.reducer

export {actions, reducer}