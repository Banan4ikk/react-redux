import {combineReducers} from 'redux';

import * as columns from './Column';
import * as cards from './Card';
import * as comments from './Comment';
import * as author from './Author';

export const reducer = combineReducers({
  cards: cards.reducer,
  columns: columns.reducer,
  comments: comments.reducer,
  author: author.reducer,
});

export const actions = {
  cards: cards.actions,
  columns: columns.actions,
  comments: comments.actions,
  author: author.actions,
};

export const selectors = {
  cards: cards.selectors,
  columns: columns.selectors,
  comments: comments.selectors,
  author: author.selectors,
};