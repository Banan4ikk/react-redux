import {createSlice, PayloadAction} from "@reduxjs/toolkit";
import {ICard} from "../../interfaces/interfaces";

const initialState: ICard[] = []

const cardSlice = createSlice({
  name: 'card',
  initialState,
  reducers: {
    addCard(state, {payload}: PayloadAction<{ newCard: ICard }>) {
      const {newCard} = payload

      state.push(newCard)
    },

    deleteCard(state, {payload}: PayloadAction<number>) {
      const index = state.findIndex((item) => item.id === payload);

      if (index !== -1) {
        state.splice(index, 1);
      }
    },

    changeDesc(state, {payload}: PayloadAction<{ cardId: number, newData: string }>) {
      const {cardId, newData} = payload

      const editCard = state.find((item) => item.id === cardId)

      if(editCard){
        editCard.data = newData
      }

     return state
    },

    changeTitle(state, {payload}: PayloadAction<{ cardId: number, newTitle: string }>) {
      const {cardId, newTitle} = payload

      const editCard = state.find((item) => item.id === cardId)

      if(editCard){
        editCard.title = newTitle
      }

      return state
    }
  }
})

const actions = {...cardSlice.actions}
const reducer = cardSlice.reducer

export {actions, reducer}