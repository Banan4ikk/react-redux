import {createSelector} from "reselect";
import {rootState} from "../store";

export const selectCards = (state: rootState) => state.cards;


export const getCardsByColumnId = (columnId: number) =>
  createSelector(selectCards, (state) =>
    state.filter((item) => item.columnId === columnId)
  );

export const getCardData = (cardId: number) =>
  createSelector(selectCards, (state) =>
    state.filter((item) => item.id === cardId)[0]
  );
