import {createSlice, PayloadAction} from "@reduxjs/toolkit";

const initialState: string =  ''

const authorSlice = createSlice({
  name: 'author',
  initialState,
  reducers: {
    setAuthor(state, {payload}: PayloadAction<string>) {
      if(payload){
        state = payload
      }

      return state
    }
  }
})

const actions = {...authorSlice.actions}
const reducer = authorSlice.reducer

export {actions, reducer}