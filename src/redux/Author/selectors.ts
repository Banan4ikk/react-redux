import {rootState} from "../store";

export const getAuthor = (state: rootState) => state.author;