import {IColumnData} from "../../interfaces/interfaces";
import {createSlice, PayloadAction} from "@reduxjs/toolkit";

const initialState: IColumnData[] =
  [
    {
      title: "TODO",
      id: 0
    },
    {
      title: "InProgress",
      id: 1
    },
    {
      title: "Testing",
      id: 2
    },
    {
      title: "Done",
      id: 3
    },
  ]


const columnSlice = createSlice({
  name: 'column',
  initialState,
  reducers: {
    changeTitle(state, action: PayloadAction<{ columnId: number, title: string }>) {
      const {payload} = action
      const {columnId, title} = payload

      const columnEdit = state.find(item => item.id === columnId)

      if (columnEdit) {
        columnEdit.title = title
      }

      return state
    },
  }
})

const actions = {...columnSlice.actions}
const reducer = columnSlice.reducer

export {actions, reducer}