import {rootState} from "../store";

export const selectColumns = (state: rootState) => state.columns;