import React, {FunctionComponent} from 'react';
import {FieldRenderProps} from "react-final-form";
import {StyledInput} from './styles';

interface ITextFieldProps extends FieldRenderProps<string> {
}

const EditInput: FunctionComponent<ITextFieldProps> = ({input}) => {
  return (
    <StyledInput {...input}/>
  );
};

export default EditInput;
