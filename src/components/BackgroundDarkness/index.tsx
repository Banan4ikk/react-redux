import React, {FunctionComponent} from 'react';
import {BackgroundDarknessStyles} from "./styles";

const BackgroundDarkness:FunctionComponent = ({children}) => {
  return (
    <BackgroundDarknessStyles>
      {children}
    </BackgroundDarknessStyles>
  );
};

export default BackgroundDarkness;
