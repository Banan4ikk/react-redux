import React, {FunctionComponent, useState} from 'react';
import {IColumnProps} from "./interfaces";
import {AddCard, AddCardText, ColumnHeader, StyledColumn, StyledImg} from "./styles";
import Card from "../Card";
import EditComponent from "../EditComponent";
import {useDispatch, useSelector} from "react-redux";
import {actions, selectors} from "../../redux/ducks"
import {ICard} from "../../interfaces/interfaces";


const Column: FunctionComponent<IColumnProps> = ({
                                                   columnId,
                                                   title,
                                                   setCardId,
                                                 }) => {

  const dispatch = useDispatch()
  const localCards = useSelector(selectors.cards.getCardsByColumnId(columnId))
  const author = useSelector(selectors.author.getAuthor)
  const [isEditTitle, setIsEditTitle] = useState(false);

  const onClickTitle = () => {
    setIsEditTitle(true)
  }

  const onSubmitChangeTitle = (value: string) => {
    dispatch(actions.columns.changeTitle({columnId: columnId, title: value}))
    setIsEditTitle(false)
  }

  const onAddCard = () => {
    const newCard: ICard = {
      id: new Date().getMilliseconds(),
      columnId: columnId,
      title: "default title",
      data: "",
      author: author
    }
    
    dispatch(actions.cards.addCard({newCard}))
  }

  return (
    <StyledColumn>
      {isEditTitle ?
        <EditComponent
          onSubmitForm={onSubmitChangeTitle}
          setIsEdit={setIsEditTitle}
        />
        :
        <ColumnHeader onClick={onClickTitle}>{title}</ColumnHeader>
      }
      {
        localCards.map((item) =>
          <Card
            key={item.id}
            id={item.id}
            title={item.title}
            setCardId={setCardId}
          />)
      }
      <AddCard>
        <AddCardText>
          Add card
        </AddCardText>
        <StyledImg src="img/plus-svg.svg" onClick={onAddCard} alt={'plus'}/>
      </AddCard>
    </StyledColumn>
  )
};

export default Column;
