import React, {Dispatch} from "react";

export interface IColumnProps {
  title: string
  columnId: number
  setCardId: Dispatch<React.SetStateAction<number | undefined>>
}
