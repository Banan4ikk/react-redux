export interface IAddComponentProps {
  onSubmitForm: (value: string) => void
}

export interface ISubmitValue {
  value: string
}