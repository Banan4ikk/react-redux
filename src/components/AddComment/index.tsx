import React, {FunctionComponent} from 'react';
import {Field, Form} from 'react-final-form';
import {IAddComponentProps, ISubmitValue} from "./interfaces";
import Styles from '../../styles/formStyles';
import {StyledButton, StyledForm } from './styles';
import EditInput from "../EditInput";

const AddComponent: FunctionComponent<IAddComponentProps> = ({onSubmitForm}) => {

  const onSubmit = (values: ISubmitValue) => {
    onSubmitForm(values.value)
  }

  return (
    <Styles>
      <Form
        onSubmit={onSubmit}
        render={({handleSubmit, form, values}) => (
          <StyledForm onSubmit={(event) => {
            event.preventDefault()
            handleSubmit()
            form.reset()
          }}>
            <Field
              name='value'
              component={EditInput}
              type='text'
              placeholder='Input your comment'
            />
            <StyledButton>OK</StyledButton>
          </StyledForm>
        )}
      />
    </Styles>
  )
};

export default AddComponent;
