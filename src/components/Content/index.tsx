import React, {FunctionComponent, useState} from 'react';
import {StyledContentBlock} from './styles';
import Column from "../Column";
import ModalCard from "../ModalCard";
import {useSelector} from "react-redux";
import {selectors} from "../../redux/ducks";

const Content: FunctionComponent = () => {

  const [cardId, setCardId] = useState<number | undefined>(undefined);
  const columns = useSelector(selectors.columns.selectColumns)

  return (
    <>
      <StyledContentBlock>
        {columns.map((item) =>
          <Column
            key={item.id}
            title={item.title}
            columnId={item.id}
            setCardId={setCardId}
          />
        )}
      </StyledContentBlock>
      {cardId &&
      <ModalCard
        setId={setCardId}
        columns={columns}
        cardId={cardId}
      />}
    </>
  )
};

export default Content;