import styled from "styled-components";

export const Container = styled.div`
  background-color: #fff;
  width: auto;
  height: fit-content;
  padding: 50px;
  border-radius: 10px;
`

export const DisabledButton = styled.button`
  padding: 10px 20px;
  border-radius: 10px;
  border: 2px #000 solid;
  margin-top: 10px;
  background: #b9b9b9;
  color: #757575;
  font-weight: 600;
  font-size: 1.1em;
`
