import React, {FunctionComponent, useEffect, useState} from 'react';
import {Container, DisabledButton} from './styles';
import BackgroundDarkness from "../BackgroundDarkness";
import {Field, Form} from 'react-final-form';
import {useDispatch, useSelector} from "react-redux";
import {actions, selectors} from "../../redux/ducks";
import {IAuthor} from "./interfaces";
import {StyledButton, StyledForm } from '../../styles/formStyles';
import EditInput from "../EditInput";

const AuthorModal: FunctionComponent = () => {

  const [visible, setVisible] = useState(false);
  const authorName = useSelector(selectors.author.getAuthor)
  const dispatch = useDispatch()

  useEffect(() => {
    if (authorName === '') {
      setVisible(true)
    }
  }, [authorName]);

  const onsubmit = (values: IAuthor) => {
    setVisible(false)
    dispatch(actions.author.setAuthor(values.author))
  }

  return (
    visible ?
      <Form onSubmit={onsubmit}
            render={({handleSubmit, submitting, pristine}) => (
              <BackgroundDarkness>
                <Container>
                  <StyledForm onSubmit={handleSubmit}>
                    <Field
                      name='author'
                      component={EditInput}
                      type='text'
                      placeholder={'Input your author name'}
                    />
                    {submitting || pristine ?
                      <DisabledButton type='submit' disabled={true}>Accept</DisabledButton>
                      :
                      <StyledButton type='submit'> Accept </StyledButton>
                    }
                  </StyledForm>
                </Container>
              </BackgroundDarkness>
            )}
      /> : null
  )
}

export default AuthorModal;