import React, {FunctionComponent} from 'react';
import {IEditProps, ISubmitValue} from "./interfaces";
import {Field, Form} from 'react-final-form'
import Styles, {StyledButton, StyledForm} from '../../styles/formStyles';
import TextField from "../EditTextArea";
import EditInput from "../EditInput";

const EditComponent: FunctionComponent<IEditProps> = ({
                                                        onSubmitForm,
                                                        isTextArea,
                                                        setIsEdit,
                                                      }) => {
  const onCancel = () => {
    setIsEdit(false)
  }

  const onSubmit = (values: ISubmitValue) => {
    onSubmitForm(values.value)
  }

  return (
    <Styles>
      {isTextArea ?
        <Form onSubmit={onSubmit}
              render={({handleSubmit, submitting, pristine}) => (
                <StyledForm onSubmit={handleSubmit}>
                  <Field
                    name='value'
                    type='text'
                    component = {TextField}
                  />
                  {submitting || pristine ?
                    <StyledButton type='button' onClick={onCancel}>Cancel</StyledButton>
                    :
                    <StyledButton type='submit'>OK</StyledButton>
                  }
                </StyledForm>
              )}
        />
        :
        <Form onSubmit={onSubmit}
              render={({handleSubmit, submitting, pristine}) => (
                <StyledForm onSubmit={handleSubmit}>
                  <Field
                    name='value'
                    component={EditInput}
                    type='text'
                  />
                  {submitting || pristine ?
                    <StyledButton type='button' onClick={onCancel}>Cancel</StyledButton>
                    :
                    <StyledButton type='submit'>OK</StyledButton>
                  }
                </StyledForm>
              )}
        />}
    </Styles>
  )
};

export default EditComponent;
