import React, {SetStateAction} from "react";

export interface ISubmitValue {
  value: string
}

export interface IEditProps {
  isTextArea?: boolean
  onSubmitForm: (value: string) => void
  setIsEdit: React.Dispatch<SetStateAction<boolean>>
}