import React, {FunctionComponent} from 'react';
import {HiddenUserMenu, StyledUserMenu} from './styles';
import {IUserMenuProps} from "./interfaces";
import {selectors} from "../../redux/ducks";
import {useSelector} from "react-redux";

const UserMenu: FunctionComponent<IUserMenuProps> = ({visible}) => {
  const authorName = useSelector(selectors.author.getAuthor)

  return visible ?
      <StyledUserMenu>
        <span>Your author name - {authorName}</span>
      </StyledUserMenu> :
      <HiddenUserMenu/>
};

export default UserMenu;
