import React, {FunctionComponent, useState} from 'react';
import {StyledHeader, StyledLogo, StyledUserIcon} from './styles'
import UserMenu from "../UserMenu";
import AuthorModal from "../AuthorModal";

const Header: FunctionComponent = () => {

  const [visibleUser, setVisibleUser] = useState(false);

  const onUserClick = () => {
    setVisibleUser(!visibleUser)
  }

  return (
    <StyledHeader>
      <StyledLogo>
        <img src="img/trello-logo.svg" alt={'main logo'}/>
      </StyledLogo>

      <StyledUserIcon>
        <img src="img/user-icon.svg" onClick={onUserClick} alt={'user icon'}/>
      </StyledUserIcon>
      <AuthorModal/>
      <UserMenu visible={visibleUser}/>
    </StyledHeader>
  )
};

export default Header;
