import React, {FunctionComponent} from 'react';
import {ICardProps} from "./interfaces";
import {Comments, CommentsCount, StyledCard} from "./styles";
import {selectors} from "../../redux/ducks"
import {useSelector} from "react-redux";

const Card: FunctionComponent<ICardProps> = ({title, id, setCardId}) => {

  const comments = useSelector(selectors.comments.getCommentsById(id))

  const onCardClick = () => {
    setCardId(id)
  }

  return (
    <StyledCard onClick={onCardClick}>
      {title}
      {comments.length > 0 ?
        <Comments>
          <img src={"img/comment.svg"} alt={'comment icon'}/>
          <CommentsCount>{comments.length}</CommentsCount>
        </Comments>
        : null}
    </StyledCard>
  )
};

export default Card;
