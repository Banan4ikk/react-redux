import React, {Dispatch} from "react";

export interface ICardProps {
  id: number,
  title: string,
  setCardId: Dispatch<React.SetStateAction<number | undefined>>
}
