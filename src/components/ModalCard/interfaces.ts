import React, {Dispatch} from "react";
import {IColumnData} from "../../interfaces/interfaces";

export interface IModalCardProps {
  setId: Dispatch<React.SetStateAction<number | undefined>>
  columns: IColumnData[]
  cardId: number
}
