import React, {FunctionComponent, useEffect, useState} from 'react';
import {
  CardAuthor,
  CardComments,
  CardData,
  CardHeader,
  CardTitle,
  CommentAuthor,
  CommentsContainer,
  Container,
  DeleteCard,
  EditText,
  StyledImg,
  StyledImgDeleteComment,
  StyledModalCard,
  StyledText
} from "./styles";
import {IModalCardProps} from "./interfaces";
import BackgroundDarkness from "../BackgroundDarkness";
import {useDispatch, useSelector} from "react-redux";
import AddComment from '../AddComment';
import {actions, selectors} from "../../redux/ducks";
import EditComponent from '../EditComponent';
import {ICommentsData} from "../../interfaces/interfaces";

const ModalCard: FunctionComponent<IModalCardProps> = ({setId, cardId}) => {

  const localCardsData = useSelector(selectors.cards.getCardData(cardId))
  const {title, data} = localCardsData

  const localComments = useSelector(selectors.comments.getCommentsById(cardId))
  const columns = useSelector(selectors.columns.selectColumns)

  const [colTitle, setColTitle] = useState<string>("");
  const [inputValue, setInputValue] = useState('');
  const [isEditData, setIsEditData] = useState(false);
  const [isEditComment, setIsEditComment] = useState(false);
  const [isEditTitle, setIsEditTitle] = useState(false);
  const [commentId, setCommentId] = useState(-1);
  const author = useSelector(selectors.author.getAuthor)
  const dispatch = useDispatch()

  useEffect(() => {
    getColTitle()
  }, [])

  const getColTitle = () => {
    const id = localCardsData.columnId
    const titleObj = columns.find(item => item.id === id)
    if (titleObj !== undefined) {
      setColTitle(titleObj.title)
    }
  }
  const onClose = () => {
    setId(undefined)
  }
  const onDeleteCard = () => {
    dispatch(actions.cards.deleteCard(cardId))
    onClose()
  }

  const onSubmitChangeTitle = (value: string) => {
    dispatch(actions.cards.changeTitle({cardId: cardId, newTitle: value}))
    setIsEditTitle(false)
  }

  const onClickTitle = () => {
    setIsEditTitle(true)
  }

  const onAddData = () => {
    setIsEditData(true)
  }

  const onDeleteData = () => {
    dispatch(actions.cards.changeDesc({cardId, newData: ""}))
  }

  const onSubmitChangeData = (value: string) => {
    dispatch(actions.cards.changeDesc({cardId, newData: value}))
    setIsEditData(false)
  }

  const onClickData = () => {
    setIsEditData(true)
  }

  const onClickComment = (id: number) => {
    setIsEditComment(true)
    setCommentId(id)
  }

  const onSubmitEditComment = (value: string) => {
    setIsEditComment(false)
    dispatch(actions.comments.changeComment({commentId, newComment: value}))
  }

  const onSubmitAddComment = (value: string) => {
    const newComment: ICommentsData = {
      id: new Date().getMilliseconds(),
      cardId: cardId,
      name: author,
      comment: value
    }
    dispatch(actions.comments.addComment({newComment}))
  }

  const onDeleteComment = (commentId: number) => {
    dispatch(actions.comments.deleteComment({commentId}))
  }

  return (
    <BackgroundDarkness>
      <StyledModalCard>
        <CardHeader>
          <CardTitle>
            {isEditTitle ?
              <EditComponent
                onSubmitForm={onSubmitChangeTitle}
                setIsEdit={setIsEditTitle}
              /> :
              <Container onClick={onClickTitle}>{title}</Container>}
          </CardTitle>
          <StyledImg src="img/cross.svg" onClick={onClose} alt={'cross'}/>
        </CardHeader>
        <CardAuthor>
          {author}
        </CardAuthor>
        <div>in column {colTitle}</div>
        {isEditData ?
          <EditComponent
            onSubmitForm={onSubmitChangeData}
            isTextArea={true}
            setIsEdit={setIsEditData}
          />
          :
          data === "" ?
            <EditText onClick={onAddData}> Add description </EditText>
            :
            <>
              <CardData onClick={onClickData}>
                <CardTitle>
                  Description:
                </CardTitle>
                <StyledText>{data}</StyledText>
              </CardData>
              <EditText onClick={onDeleteData}>Remove description</EditText>
            </>}
        <DeleteCard onClick={onDeleteCard}>
          Delete card
        </DeleteCard>
        <CardComments>
          <CardTitle>
            Comments:
          </CardTitle>
          {localComments.map((item) =>
            <CommentsContainer key={item.id}>
              <CardComments>
                <CommentAuthor>
                  {item.name}
                </CommentAuthor>
                {isEditComment && item.id === commentId ?
                  <EditComponent
                    onSubmitForm={onSubmitEditComment}
                    setIsEdit={setIsEditComment}
                    key={item.id}
                  />
                  :
                  <StyledText onClick={() => onClickComment(item.id)}>
                    {item.comment}
                  </StyledText>}
              </CardComments>
              <StyledImgDeleteComment src={"img/cross.svg"} onClick={() => onDeleteComment(item.id)} alt={'cross'}/>
            </CommentsContainer>
          )}
          <CardTitle>Add comment</CardTitle>
          <AddComment onSubmitForm={onSubmitAddComment}/>
        </CardComments>
      </StyledModalCard>
    </BackgroundDarkness>
  )
};

export default ModalCard;
