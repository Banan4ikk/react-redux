import React, {FunctionComponent} from 'react';
import {FieldRenderProps} from "react-final-form";
import {StyledTextArea} from './styles';

interface ITextFieldProps extends FieldRenderProps<string> {}

const TextField: FunctionComponent<ITextFieldProps> = ({input}) => {
  return (
    <StyledTextArea {...input}/>
  );
};

export default TextField;
