import React from 'react';
import AppWrapper from "./components/AppWrapper";
import Header from "./components/Header";
import Content from "./components/Content";
import {Provider} from "react-redux";
import {PersistGate} from "redux-persist/integration/react";
import {persistor, store} from "./redux/store";

function App() {

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <AppWrapper>
          <Header/>
          <Content/>
        </AppWrapper>
      </PersistGate>
    </Provider>
  );
}

export default App;
