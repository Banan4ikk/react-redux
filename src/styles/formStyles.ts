import styled from "styled-components";

export default styled.div`

`

export const StyledButton = styled.button`
  padding: 10px 20px;
  border-radius: 10px;
  border: 2px #000 solid;
  margin-top: 10px;
  background: #fff;
  font-weight: 600;
  font-size: 1.1em;
  transition: background-color 0.1s ease-in-out;

  &:hover {
    cursor: pointer;
    background-color: #e8e8e8;
  }
`

export const StyledForm = styled.form`
  display: flex;
  justify-content: center;
  flex-direction: column;
`